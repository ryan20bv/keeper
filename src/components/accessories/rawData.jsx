const item1 = {
	id: 95137,
	title: "What is Lorem Ipsum?",
	content:
		"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
};
const item2 = {
	id: 957364,
	title: "Why do we use it?",
	content:
		"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
};
const item3 = {
	id: 9513545,
	title: "Where can I get some?",
	content:
		"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
};

const defaultItems = [item1, item2, item3];

export default defaultItems;
